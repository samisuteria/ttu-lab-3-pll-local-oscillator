#include <msp430g2553.h>

//Ports
#define P1_0 0x01
#define P1_1 0x02
#define P1_2 0x04
#define P1_3 0x08
#define P1_4 0x10
#define P1_5 0x20
#define P1_6 0x40
#define P1_7 0x80
#define P2_0 0x01
#define P2_1 0x02
#define P2_2 0x04
#define P2_3 0x08
#define P2_4 0x10
#define P2_5 0x20
//#define P2_6 0x40
//#define P2_7 0x80

// 0000 0000
// 7654 3210


//MSP430G2553 is 1MHz
//MSP430F5229 is 25MHz

const int CLOCK = 1000;


void delay(unsigned int milliseconds){
    while (milliseconds--) {
        __delay_cycles(CLOCK);
    }
}

void blink(int x){
    
    //set LED(P1.0) as output and turn it on then blink it x times
    P1DIR |= P1_0;
    
    for(int i=0; i< x*2; i++){
        P1OUT ^= P1_0;
        delay(300);
    }
    
    
}

void scanKeypad(){
    /*
        Key/Pin Combos
        
        1(1,5) 2(1,6) 3(1,7)
        4(2,5) 5(2,6) 6(2,7)
        7(3,5) 8(3,6) 9(3,7)
        *(4,5) 0(4,6) #(4,7)
     
        Pin 1 -> P1.4
        Pin 2 -> P1.5
        Pin 3 -> P2.0
        Pin 4 -> P2.1
        Pin 5 -> P2.2
        Pin 6 -> P2.5
        Pin 7 -> P2.4
    */
    
    //Make Column Pins as Outputs
    P2DIR |= P2_2 + P2_5 + P2_4;
    
    //Turn on pulldown resistors on rows
    P1REN |= P1_4 + P1_5;
    P2REN |= P2_0 + P2_1;
    
    //Set the first column high (Pin 5) and turn other colums Low and wait for it to get there
    P2OUT |= P2_2;
    P2OUT &= ~(P2_5 + P2_4);
    delay(1);
    
    //check if 1,4,7 or * is pressed
    if(P1IN & P1_4){
        blink(1);
    }
    if(P1IN & P1_5){
        blink(4);
    }
    if(P2IN & P2_0){
        blink(7);
    }
    if(P2IN & P2_1){
        blink(0);
    }
    
    //Set the second column high (Pin 6) and turn other colums Low and wait for it to get there
    P2OUT |= P2_5;
    P2OUT &= ~(P2_2 + P2_4);
    delay(1);
    
    //check if 2,5,8 or 0 is pressed
    if(P1IN & P1_4){
        blink(2);
    }
    if(P1IN & P1_5){
        blink(5);
    }
    if(P2IN & P2_0){
        blink(8);
    }
    if(P2IN & P2_1){
        blink(0);
    }
    
    //Set the third column high (Pin 7) and turn other colums Low and wait for it to get there
    P2OUT |= P2_4;
    P2OUT &= ~(P2_2 + P2_5);
    delay(1);
    
    //check if 3,6,9 or # is pressed
    if(P1IN & P1_4){
        blink(3);
    }
    if(P1IN & P1_5){
        blink(6);
    }
    if(P2IN & P2_0){
        blink(9);
    }
    if(P2IN & P2_1){
        blink(0);
    }
     
    delay(100);
    
    
}


int main(void){
    //turn off watch dog timer (idk why - everyone does it)
    WDTCTL = WDTPW + WDTHOLD;
    //set all pins as input and all outputs as low and turn off all pullup/pulldown resistors
    P1DIR = 0x00;
    P2DIR = 0x00;
    P1OUT = 0x00;
    P2OUT = 0x00;
    P1REN = 0x00;
    P2REN = 0x00;
    
    
    for (;;) {
        scanKeypad();
    }
    
    return 0;
}




/* HEX to BIN

*/